# # UpdateConsumer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**shipping_address** | [**\Klarna\Model\Address**](Address.md) |  | [optional] 
**billing_address** | [**\Klarna\Model\Address**](Address.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


