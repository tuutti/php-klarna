# # Location

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | The location id | [optional] 
**name** | **string** | The display name of the location | [optional] 
**price** | **int** | The price for this location | [optional] 
**address** | [**\Klarna\Model\Address**](Address.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


