## Requirements

- [openapi-generator-cli](https://github.com/OpenAPITools/openapi-generator)
- [openapi-merge-cli](https://www.npmjs.com/package/openapi-merge-cli)


## Download API specs

- `https://developers.klarna.com/api/specs/payments.json`
- `https://developers.klarna.com/api/specs/ordermanagement.json`

## Changes made to payments.json

1. Search and replaced all `orders` tags to `payment-orders`.
2. Replaced `#/definitions/order` with `#/definitions/payment-order`
3. Added security definitions

```
"securityDefinitions": {
  "basicAuth": {
    "type": "basic"
  }
},
"security": [
  {
    "basicAuth": []
  }
],
```

## Generate the API client

Run `build/generate.sh`.
