#!/bin/bash

# Convert Swagger 2.0 specs to OAS3
for NAME in payments ordermanagement
do
  openapi-generator-cli generate -g openapi-yaml -i $NAME.json -o .
  cp ./openapi/openapi.yaml $NAME.yaml
done

# Merge all specs into one.
openapi-merge-cli

# Generate the client.
openapi-generator-cli generate -c config.json -i output.json -g php -o ../ --skip-validate-spec --git-host=gitlab.com --git-repo-id=php-klarna --git-user-id=tuutti
